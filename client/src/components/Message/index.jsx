import React, { useState } from 'react';
import { Input, Button, Comment } from 'semantic-ui-react';
import { Mutation } from 'react-apollo';
import { GET_MESSAGES, POST_MESSAGE } from '../../queries';

import styles from './styles.module.scss';
import ReactionButton from '../ReactionButton';

const Message = ({ message, filter, skip, first, orderBy }) => {
    const [replyText, setReplyText] = useState('');
    const [isEdited, setIsEdited] = useState(false);
    const { id, text, replyTo, likeCount, dislikeCount } = message;

    const onChange = (event) => setReplyText(event.target.value);

    const toggleEditState = () => {
        setIsEdited(!isEdited);
        setReplyText('');
    };

    const updateCacheWithNewMessage = (cache, newMessage) => {
        const data = cache.readQuery({
            query: GET_MESSAGES,
            variables: {
                filter,
                skip,
                first,
                orderBy
            }
        });
        cache.writeQuery({
            query: GET_MESSAGES,
            data: {
                messages: {
                    messageList: [...data.message.messageList, newMessage]
                },
                count: data.messages.count + 1
            }
        });
    };

    return (
        isEdited
            ? (
                <Mutation
                    mutation={POST_MESSAGE}
                    update={(cache, { data: { postMessage } }) => {
                        updateCacheWithNewMessage(cache, postMessage);
                    }}
                    variables={{
                        userId: localStorage.getItem('userId'),
                        text: replyText,
                        replyTo: id
                    }}
                >
                    {postMessage => (
                        <form className={styles.messageForm} onSubmit={event => {
                            event.preventDefault();

                            if (!replyText) return;
                            postMessage();
                            toggleEditState();
                        }}>
                            <Input
                                action={{
                                    color: 'blue',
                                    content: 'Reply',
                                    disabled: !replyText
                                }}
                                defaultValue={replyText}
                                placeholder={`Reply to #${id}`}
                                onChange={onChange}
                            />
                            <Button color="red" onClick={toggleEditState}>Cancel</Button>
                        </form>
                    )}
                </Mutation>
            )
            : (
                <Comment>
                    <Comment.Content>
                        <Comment.Metadata>
                            #{id.toString().padStart(3, '0')} {replyTo && `Reply to #${replyTo.toString().padStart(3, '0')}`}
                        </Comment.Metadata>
                        <Comment.Text>
                            {text}
                        </Comment.Text>
                        <Comment.Actions>
                            <ReactionButton counter={likeCount} isLike={true} messageId={id} filter={filter} skip={skip} first={first} orderBy={orderBy} />
                            <ReactionButton counter={dislikeCount} isLike={false} messageId={id} filter={filter} skip={skip} first={first} orderBy={orderBy}/>
                            {!replyTo && (
                                <Comment.Action onClick={toggleEditState}>
                                    Reply
                                </Comment.Action>
                            )}
                        </Comment.Actions>
                    </Comment.Content>
                </Comment>
            )
    );
};

export default Message;