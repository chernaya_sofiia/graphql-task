import React from 'react';
import PropTypes from 'prop-types';
import { Query } from 'react-apollo';
import { Pagination, Dimmer, Loader, Comment } from 'semantic-ui-react';
import { GET_MESSAGES, NEW_MESSAGE_SUBSCRIPTION } from '../../queries';
import Message from '../Message';

const MessageList = ({ filter, skip, first, orderBy, messagePerPage, onPageChange }) => {
    const subscribeToNewMessages = (subscribeToMore) => {
        subscribeToMore({
            document: NEW_MESSAGE_SUBSCRIPTION,
            updateQuery: (prev, { subscriptionData }) => {
                if (!subscriptionData.data) return prev;
                const newMessage = subscriptionData.data.newMessage;

                console.log(prev.messages.messageList.length);
                console.log(prev);

                return {
                    messages: {
                        messageList: [...prev.messages.messageList, newMessage],
                        count: prev.messages.messageList.length + 1,
                        __typename: prev.messages.__typename
                    }
                };
            }
        });
    };

    return (
        <Query
            query={GET_MESSAGES}
            variables={{ filter, skip, first, orderBy }}
        >
            {({ loading, error, data, subscribeToMore }) => {
                if (loading) {
                    return (
                        <Dimmer active>
                            <Loader/>
                        </Dimmer>
                    );
                }
                if (error) {
                    console.log(error);
                    return (
                        <Message negative>
                            <Message.Header>An error occurred</Message.Header>
                        </Message>
                    );
                }
                subscribeToNewMessages(subscribeToMore);

                const { messageList, count } = data.messages;
                const totalPages = Math.ceil(count / messagePerPage);

                return (
                    <>
                        <Comment.Group>
                            {messageList.map(message => {
                                return <Message
                                    key={message.id}
                                    message={message}
                                    filter={filter}
                                    skip={skip}
                                    first={first}
                                    orderBy={orderBy}
                                />
                            })}
                        </Comment.Group>
                        <Pagination
                            defaultActivePage={1}
                            ellipsisItem={null}
                            firstItem={null}
                            lastItem={null}
                            siblingRange={3}
                            totalPages={totalPages}
                            onPageChange={onPageChange}
                        />
                    </>
                )
            }}
        </Query>
    );
};

export default MessageList;