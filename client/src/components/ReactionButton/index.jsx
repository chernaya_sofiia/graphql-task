import React from 'react';
import { Comment, Icon } from 'semantic-ui-react';
import { Mutation } from 'react-apollo';
import { GET_MESSAGES, SET_REACTION_MUTATION } from '../../queries';

const ReactionButton = ({ counter, isLike, messageId, filter, skip, first, orderBy }) => {
    const updateCacheWithNewReaction = (cache, updatedMessage, messageId) => {
        const data = cache.readQuery({
            query: GET_MESSAGES,
            variables: {
                filter,
                skip,
                first,
                orderBy
            }
        });
        const reactedPostIndex = data.messages.messageList.findIndex(message => message.id === messageId);
        data.messages.messageList[reactedPostIndex].likeCount = updatedMessage.likeCount;
        data.messages.messageList[reactedPostIndex].dislikeCount = updatedMessage.dislikeCount;
        cache.writeQuery({
            query: GET_MESSAGES,
            data
        })
    };

    return (
        <Mutation
            mutation={SET_REACTION_MUTATION}
            variables={{
                userId: localStorage.getItem('userId'),
                isLike,
                messageId
            }}
            update={(cache, { data: { setReaction } }) => {
                updateCacheWithNewReaction(cache, setReaction, messageId)
            }}
        >
            {setReaction =>
                <Comment.Action onClick={setReaction}>
                    <Icon name={`thumbs ${isLike ? 'up' : 'down'}`}/> {counter}
                </Comment.Action>
            }
        </Mutation>
    )
};

export default ReactionButton