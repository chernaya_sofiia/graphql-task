import React, { useState } from 'react';
import { Input } from 'semantic-ui-react';
import { Mutation } from 'react-apollo';
import { GET_MESSAGES, POST_MESSAGE } from '../../queries';

import styles from './styles.module.scss';

const MessageInput = ({ filter, skip, first, orderBy }) => {
    const [text, setText] = useState('');

    const onChange = (event) => setText(event.target.value);

    const updateCacheWithNewMessage = (cache, newMessage) => {
        const data = cache.readQuery({
            query: GET_MESSAGES,
            variables: {
                filter,
                skip,
                first,
                orderBy
            }
        });
        cache.writeQuery({
            query: GET_MESSAGES,
            data: {
                messages: {
                    messageList: [...data.message.messageList, newMessage]
                },
                count: data.messages.count + 1
            }
        });
    };

    return (
        <Mutation
            mutation={POST_MESSAGE}
            update={(cache, { data: { postMessage } }) => {
                updateCacheWithNewMessage(cache, postMessage);
            }}
            variables={{
                userId: localStorage.getItem('userId'),
                text
            }}
        >
            {postMessage => (
                <form className={styles.messageForm} onSubmit={event => {
                    event.preventDefault();

                    if (!text) return;
                    postMessage();
                    setText('');
                }}>
                    <Input
                        action={{
                            color: 'blue',
                            content: 'Send',
                            disabled: !text
                        }}
                        defaultValue={text}
                        placeholder="Enter message text..."
                        size="massive"
                        onChange={onChange}
                    />
                </form>
            )}
        </Mutation>
    );
};

export default MessageInput;