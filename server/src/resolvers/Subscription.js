const newMessageSubscribe = (parent, args, context) => {
    return context.prisma.$subscribe.message({
        mutation_in: ['CREATED', 'UPDATED']
    }).node();
};

const newReactionSubscribe = (parent, args, context) => {
    return context.prisma.$subscribe.messageReaction({
        mutation_in: ['CREATED', 'UPDATED', 'DELETED']
    }).previousValues();
};

const newMessage = {
    subscribe: newMessageSubscribe,
    resolve: payload => payload
};
const newReaction = {
    subscribe: newReactionSubscribe,
    resolve: payload => payload
};

module.exports = { newMessage, newReaction };
