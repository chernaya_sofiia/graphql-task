const reactions = (parent, args, context) => {
    return context.prisma.messageReactions({
        where: {
            message: {
                id: parent.id
            }
        }
    });
};

const likeCount = async (parent, args, context) => {
    return context.prisma
        .messageReactionsConnection({ where: { message: { id: parent.id }, isLike: true }})
        .aggregate()
        .count();
};

const dislikeCount = async (parent, args, context) => {
    return context.prisma
        .messageReactionsConnection({ where: { message: { id: parent.id }, isLike: false }})
        .aggregate()
        .count();
};

module.exports = { reactions, likeCount, dislikeCount };